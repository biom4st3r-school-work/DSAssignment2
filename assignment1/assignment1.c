#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main(int count, char ** chars) {
  printf("Curtis Harris, tto109\n");
  printf("Enter 6 parenthesis: ");
  Node* stack = push(NULL, getchar());
  for(int i = 0; i < 5; i++) {
    stack = push(stack,getchar());
  }
  int state = 0;
  if (peek(stack) == '{') {
    printf("Invalid parenthesis Expression\n");
    return -1;
  } else {
    printf("Stack full, last value enter(stack top): %c\n", stack->c);
  }
  for (int i = 0; i < 6; i++) {
    char c = peek(stack);
    stack = pop(stack);
    switch (c) {
      case '{':
        state += 1;
        break;
      case '}':
        state -= 1;
        break;
      default:
        printf("This progarm only accepts '{' and '}' not '%c'", c);
        return -1;
    }
  }
  
  
  if (state == 0) {
    printf("Valid Parenthesis Expression\n");
    return 0;
  }
  printf("Invalid Paraenthesis Expression\n");
  
}
