
typedef struct Node {
  struct Node* parent;
  char c;
  
} Node;

Node* push(Node* body, char c);
Node* pop(Node* body);
char peek(Node* body);