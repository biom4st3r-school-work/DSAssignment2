#include "stack.h"
#include <stdlib.h>

Node* push(Node* body, char c) {
  Node* new_node = (Node*)malloc(sizeof(Node));
  new_node->c = c;
  new_node->parent = body;
  return new_node;
}
Node* pop(Node* body) {
   Node* parent = body->parent;
  // (*out) = body->c;
  free(body);
  return parent;
}
char peek(Node* body) {
  return body->c;
}
