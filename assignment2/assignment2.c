#include <stdio.h>
#define Approach "Blank"
#include "binarysearch.h"
#include <time.h>


void sort(int* vals, int size) {
  for (int i = 1; i < 5; i++) {
    int start = i;
    while (start > 0 && vals[start] < vals[start - 1]  ) {
      int t = vals[start-1];
      vals[start - 1] = vals[start];
      vals[start] = t;
      start -= 1;
    }
  }
}

int main(int count, char** args) {
  printf("Curtis Harris, tto109\n");
  printHeader();
  int vals[5];
  printf("\nEnter 5 numbers: ");
  for (int i = 0; i < 5; i++ ) {
    scanf("%d", vals+i);
  }
  int search = 0;
  printf("\nEnter element to search for: ");
  scanf("%d", &search);
  sort(vals, 5);
  // for ( int i = 0; i < 5; i++) {
  //   printf("%d, ", vals[i]);
  // }
  clock_t start_t = clock();
  int index = binary_search(search, vals, 5);
  clock_t end_t = clock();
  double total = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("Total Time taken by CPU (End Time - Start Time)/clock per_sec: %lf\n", total);
  if (index == -1) {
    printf("\nCould not find %d", search);
    return -1;
  } else {
    printf("%d @ index %d\n", search, index);
  }
}
