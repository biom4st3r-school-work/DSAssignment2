#include "binarysearch.h"
#include <stdio.h>

void printHeader() {
  printf("Binary Search (Recursive approach)\n");
}

int _interal_binary_search(int search, int* vals, int end, int start) {
  if (start <= end) {
    int i = (start + end) / 2;
    if (vals[i] > search) {
      return _interal_binary_search(search, vals, i-1, start);
    } else if (vals[i] < search) {
      return _interal_binary_search(search, vals, end, i+1);
    } else {
      return i;
    }  
  }
  return -1;
}

int binary_search(int search, int* vals, int size ) {
  return _interal_binary_search(search, vals, size, 0);
}

