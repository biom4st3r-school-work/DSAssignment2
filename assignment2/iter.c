#include "binarysearch.h"
#include <stdio.h>

void printHeader() {
  printf("Binary Search (Iterative approach)\n");
}

int binary_search(int search, int* vals, int size ) {
  int h = size-1;
  int l = 0;
  
  while (l <= h) {
    // printf("h%d l%d\n", h, l);
    int curridx = (h + l) / 2;
    const int current = vals[curridx];
    if (current > search) {
      h = curridx-1;
    } else if (current < search) {
      l = curridx+1;
    } else {
      return curridx;
    }
  }
  return -1;
}